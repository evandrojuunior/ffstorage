import React, {useState, useEffect} from 'react'
import FormRegister from './FormRegister'
import fireDB from '../firebase'

const Register = () => {

    let [data_Item, setData_Item] = useState({})

    let [idCurrent, setIdCurrent] = useState('');

    useEffect( () => {
        fireDB.child('estoque').on('value', dbPhoto => {
            if(dbPhoto.val() != null){
                setData_Item({
                    ...dbPhoto.val()
                })
            }else{
                setData_Item({});
            }
        })
    }, [])

    const addEdit = obj => {

        if(idCurrent == ''){
            fireDB.child('estoque').push(
                obj,
                error =>{
                    if(error){
                        console.log(error);
                    }
                }
            )
        }else{
            fireDB.child(`estoque/${idCurrent}`).set(
                obj,
                err =>{
                    if(err){
                        console.log(err);
                    }
                }
            )
            window.location.reload();
        }

    }

    const deleteItem = key => {
        if(window.confirm('Deseja realmente deletar?')) {
            fireDB.child(`estoque/${key}`).remove()
        }
    }


    return (
        <div>
            <div className="row align-items-md-stretch">
                <div className="col-md-12">
                    <div className="h-100 p-5 text-white bg-dark rounded-3">
                        <h1 className="text-center">ESTOQUE FF</h1>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">
                    <FormRegister {...({addEdit, idCurrent, data_Item})} />
                </div>
                <div className="col-md-7">
                    <table className="table table-bordeless table-stripped">
                        <thead className="thead-light">
                            <tr>
                                <td>Quantidade</td>
                                <td>Nome do Item</td>
                                <td>Nome da Marca</td>
                                <td>Valor de Compra</td>
                                <td>Valor de Venda</td>
                                <td>Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                Object.keys(data_Item).map(id => {
                                    return <tr key={id}>
                                        <td>{data_Item[id].quantity}</td>
                                        <td>{data_Item[id].nameItem}</td>
                                        <td>{data_Item[id].nameBrand}</td>
                                        <td>{data_Item[id].valueBuy}</td>
                                        <td>{data_Item[id].valueSell}</td>
                                        <td>
                                            <a className="btn btn-primary" onClick={() => {setIdCurrent(id)}}>
                                                <i className="fas fa-pencil-alt"></i>
                                            </a>
                                            <a className="btn btn-danger" onClick={() => deleteItem(id)}>
                                                <i className="fas fa-trash-alt"></i>
                                            </a>
                                            
                                        </td>
                                    </tr>
                                })
                            }
                        </tbody>

                    </table>
                </div>
                
            </div>

        </div>
    )
}

export default Register;