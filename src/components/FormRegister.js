import React, { useEffect, useState } from 'react'


const FormRegister = (props) => {

    //var de capdados

    const data_value = {
        nameItem: '',
        nameBrand: '',
        valueBuy: '',
        valueSell: '',
        quantity: ''

    }

    let [values, setValues] = useState(data_value);

    const manipulatorInputChange = e => {
        let { name, value } = e.target

        setValues({
            ...values,
            [name]: value
        })
    }

    const manipulatorFormSubmit = e => {
        e.preventDefault();
        props.addEdit(values);
    }

    useEffect( () => {
        if(props.idCurrent == ''){
            setValues({
                ...data_value
            })
        }else{
            setValues({
                ...props.data_Item[props.idCurrent]
            })
        }
    }, [props.idCurrent, props.data_Item])

    return (
        <div>
            <form autoComplete="off" onSubmit={manipulatorFormSubmit}> 
                <div className="row">
                    
                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i>NOME DO ITEM</i>
                        </div>
                    </div>
                    <input className="form-control text-uppercase" name="nameItem" value={values.nameItem} onChange={manipulatorInputChange} />
                </div>

                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i>NOME DA MARCA</i>
                        </div>
                    </div>
                    <input className="form-control text-uppercase" name="nameBrand" value={values.nameBrand} onChange={manipulatorInputChange} />
                </div>

                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i>VALOR DE COMPRA</i>
                        </div>
                    </div>
                    <input className="form-control text-uppercase" name="valueBuy" value={values.valueBuy} onChange={manipulatorInputChange}/>

                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i>VALOR DE VENDA</i>
                        </div>
                    </div>
                    <input className="form-control text-uppercase" name="valueSell" value={values.valueSell} onChange={manipulatorInputChange}/>

                </div>

                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i>QUANTIDADE</i>
                        </div>
                    </div>
                    <input className="form-control text-uppercase" name="quantity" value={values.quantity} onChange={manipulatorInputChange}/>
                </div>

                </div>
                <div className="form-group d-grid gap-2">
                    <input type="submit" value={ props.idCurrent == '' ? 'Salvar' : 'Atualizar'} className="btn btn-primary"></input>
                </div>
            </form>
        </div>
    )
}

export default FormRegister;