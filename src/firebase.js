import firebase from 'firebase'

  const firebaseConfig = {
    apiKey: "AIzaSyCn3RXaCum85_t-3R8r7x29xeuUKrT4_D8",
    authDomain: "ffstorage-7c3e0.firebaseapp.com",
    projectId: "ffstorage-7c3e0",
    storageBucket: "ffstorage-7c3e0.appspot.com",
    messagingSenderId: "658567543957",
    appId: "1:658567543957:web:b00017db644a9b78afef9c",
    measurementId: "G-HNQMS7LSTB"
  };

  let fireDB = firebase.initializeApp(firebaseConfig);

  export default fireDB.database().ref();