import './App.css';
import Register from './components/Register';

function App() {
  return (
    <div className="col-md-10 offset-md-1">
      <Register />
    </div>
  );
}

export default App;
